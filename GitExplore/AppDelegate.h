//
//  AppDelegate.h
//  GitExplore
//
//  Created by Datou on 2018/4/2.
//  Copyright © 2018 OE. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

