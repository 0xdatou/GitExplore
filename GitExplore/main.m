//
//  main.m
//  GitExplore
//
//  Created by Datou on 2018/4/2.
//  Copyright © 2018 OE. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
